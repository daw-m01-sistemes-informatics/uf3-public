### Exercicis Màquines virtuals

Introducció: creació de Màquines Virtuals

##### Exercici 1

Utilitzeu la xuleta d'[instal·lació de
kvm](https://gitlab.com/hisx-m05-fonaments-maquinari/uf3-public/raw/master/1-Maquines-Virtuals/instal_kvm.md)
per poder virtualitzar amb KVM i posteriorment creeu amb *virt-manager* alguna
màquina virtual a partir d'alguna _*.iso_ de les que es pot trobar a *gandhi*
consensuada amb el professor.

##### Exercici 2

El programa *virt-builder* ens permet construir VM d'una manera molt ràpida.
Anem a treballar aquesta eina:

* Si no el tenim instal·lat, mira a quin paquet pertany i instal·la-la:

* Llistem totes les imatges VM disponibles:

* Mostrem ara les notes d'instal·lació d'Ubuntu 20.04

* Quina ordre instal·la un fedora 32?

* Quin és el directori habitual a on es desen les _plantilles_ de les imatges
  baixades?

* Per defecte la imatge creada serà un fitxer _*.img_. Si vull canviar el nom
  del fitxer, quina opció he de fer servir?

* I si vull canviar el format? _qcow2_ per exemple.

* Quina ordre m'instal·la un ubuntu 20.04 amb:

	* contrasenya de root *jupiter*
	* amb nom *ubuntu-20_04.qcow2*
	* al directori /var/lib/libvirt/images/ (tot i que virt-builder no necessita permís de root copiar en aquest directori¿?)
	* format _qcow2_
	* hostname _virt.example.com_
	* instal·lant l'escriptori gràfic _lxde_ 
	* actualitzant tots els paquets

##### Exercici 3

Creem amb virt-manager una VM a patir de la imatge Centos minimal de tipus
qcow2 que es troba a `/home/groups/inf/public/install/VM/`

------

*Tipus de xarxes a les VM*

##### Exercici 4

A partir de qualsevol de les VM instal·lades als exercicis anteriors, quina és
la configuració (el tipus) de xarxa per defecte?

Quina IP té el convidat?

Quin *default gateway* té el convidat?

Feu ping des de l'amfitrió al convidat:

Feu ping des del convidat a l’amfitrió

Feu ping des del convidat a un ordinador de la xarxa de classe


Penseu que si esteu a casa podeu fer ping a l'adreça IP d'un dispositiu mòbil
connectat a la vostra xarxa d'àrea local (se suposa que al vostre router).

Un cop comproveu que efectivament us fa _reply_ del _request_ de ping, podeu jugar
posant en mode avió o desconnectant del wifi el vostre smartphone per veure com
ja no hi ha resposta.


Feu ping des del convidat a internet (se suposa que hi ha internet des de
l'amfitrió)

Feu ping des d'un altre ordinador de la xarxa de classe a l'ordinador convidat 


##### Exercici 5

*[ALERTA: Per fer aquest exercici heu d'utilitzar al vostre ordinador amfitrió
una interfície de xarxa ethernet, no pot ser wireless, tal i com podeu llegir a
una de les notes importants del
manual](https://wiki.libvirt.org/page/Networking#Bridged_networking_.28aka_.22shared_physical_device.22.29)*


Canvieu ara la xarxa per defecte que hi ha a una VM i feu servir una xarxa
bridge amb *macvtap*.

Quina IP té l'amfitrió del convidat

Quin *default gateway* té el convidat?

Feu ping des de l'amfitrió al convidat:

Feu ping des del convidat a l’amfitrió.

Feu ping des del convidat a un ordinador de la xarxa de classe.

Feu ping des del convidat a internet (se suposa que hi ha internet des de l'amfitrió)

Feu ping des d'un altre ordinador de la xarxa de classe a l'ordinador convidat

##### Exercici 6

Creeu una xarxa virtual *isolated* `192.168.150.0/24` amb DHCP i configureu
dues màquines (clonant una abans si és necessari) responeu a les mateixes
preguntes:

Quina IP té l'amfitrió del convidat

Quin *default gateway* té el convidat

Feu ping des de l'amfitrió al convidat:

Feu ping des del convidat a l’amfitrió

Feu ping des del convidat a un ordinador de la xarxa de classe

Feu ping des del convidat a internet (se suposa que hi ha internet des de l'amfitrió)

Feu ping des d'un altre ordinador de la xarxa de classe a l'ordinador convidat

##### Exercici 7 (virsh)

* Com podem veure amb l'ordre `virsh` totes les xarxes virtuals que gestiona
libvirtd? Opció interactiva i no interactiva.

* Com desactivem una xarxa?
	
* Com llistem totes les xarxes, tant actives com inactives?

* Com activem una xarxa inactiva?


