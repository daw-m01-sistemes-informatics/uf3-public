# Instal·lació de kvm a un GNU/Linux (Debian)

## Requeriments previs

Abans d'instal·lar al nostre ordinador qualsevol programari de virtualització
hauríem de comprovar si el nostre maquinari (*hardware*) ens ho permet. No tots
el pc's admeten virtualització per KVM.

Alerta al cas d'Intel: VT (Virtualization Technology) és un terme que acull la
virtualització en sentit general i VT-x era una de les solucions , com VT-d és
una altra més moderna i que no és incompatible amb l'anterior (ni tampoc
necessària). Per tant, podem tenir alguna BIOS amb el bit de VT activat i el de
VT-d desactivat i podrem virtualitzar (encara que sigui amb menys *features*).
[Més info en aquest
enllaç](https://web.archive.org/web/20150319213640/https://software.intel.com/en-us/blogs/2009/06/25/understanding-vt-d-intel-virtualization-technology-for-directed-io/)

Comprovem si la cpu té activades certes banderes: *vmx* (*intel*) o *svm*
(*amd*). Per fer això executem (no cal ser *root*):

```
egrep '^flags.*(vmx|svm)' /proc/cpuinfo
```

Si la consola mostra alguna cosa, podrem *virtualitzar* amb *KVM*, si no surt
res no podrem.

D'altra banda necessitarem tenir el medi que conté el sistema operatiu que
volem instal·lar virtualment, ja sigui una imatge *iso*, un dvd, una *url* ...

## Instal·lació 

Ara sí com a root, instal·lem els grup de paquets de virtualització:

```
apt install qemu-kvm libvirt-daemon bridge-utils virtinst libvirt-daemon-system virt-manager
```

Podem comprovar que el dimoni `libvirtd` ja està arrencat amb la següent ordre:

```
systemctl status libvirtd
```

Comprovem que els mòduls del *kernel* per a *KVM* s'han carregat:

```
lsmod | grep kvm
```

Si es mostra informació on surt o *kvm_amd* o *kvm_intel* anem bé. En cas
contrari haurem d'anar a la BIOS a activar VT.

## Configuració


### Directori d'imatges

Es recomanable que controlem l'espai que ens consumiran les diferents imatges
virtuals, ja que per defecte el directori emprat pot ser un que no s'adapti al
nostre disseny d'espai d'emmagatzemmatge.

Depenent de la situació que tinguem pot ser interessant canviar el directori de
les imatges.

[Canvi de directori a on es guarden les imatges ben
fet](https://www.unixarena.com/2015/12/linux-kvm-change-libvirt-vm-image-store-path.html/)

Una altra solució menys ortodoxa pots ser crear el directori d'imatges a una
partició de dades i fer que el directori habitual d'imatges sigui un enllaç
simbòlic.

### Grup libvirt

Si volem gestionar màquines virtuals amb un usuari ordinari (no root) hauríem
d'afegir-lo al grup libvirt:

```
adduser el_teu_usuari libvirt
```

## Execució

Executem un dels programes gràfics que ens permet gestionar les màquines
virtuals: 

```
virt-manager
```

L'ordre anterior, s'ha executat des d'una consola però també es pot executar
des de l'escriptori

Un cop establim una connexió ja podrem construir una màquina virtual des de
diferents tipus d'instal·lació o fins i tot utilitzant una imatge que ja
tinguem feta (última opció a la captura de pantalla)

![Pas 1 en la creació d'una màquina virtual](virt-manager-pas1.png)


## FONTS

+ [Enllaç principal de KVM a Debian](https://wiki.debian.org/KVM)

